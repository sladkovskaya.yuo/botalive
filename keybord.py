from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from telebot import types

def keybord(b_name):
    num = len(b_name)
    mark = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for i in range(num):
        print(num,b_name)
        b = types.KeyboardButton(b_name[i])
        mark.add(b)
    return mark

def keybord_next(b_name):
    num = len(b_name)
    b = []
    mark = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for i in range(num):
        print(num,b_name)
        b.append(types.KeyboardButton(b_name[i]))
    mark.row(*b)
    return mark
