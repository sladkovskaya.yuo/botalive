import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
import json
from sqlalchemy.orm import sessionmaker,relationship
import os
import shutil
from datetime import datetime

Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)

class BotData(Common, Base):
    __tablename__ = "UsersMapping"
    obj = dict()
    subscription_name = sa.Column(sa.String, unique=False, nullable=False)
    subscriber_name = sa.Column(sa.String, unique=False, nullable=False)
    subscription_chat = sa.Column(sa.String, unique=False, nullable=True)
    id_sbs = sa.Column(sa.String, unique=True, nullable=False)

class BotDataUsersChat(Common, Base):
    __tablename__ = "UsersChat"
    obj = dict()
    name = sa.Column(sa.String, unique=False, nullable=False)
    chatId = sa.Column(sa.String, unique=False, nullable=False)
    id_sbs = sa.Column(sa.String, unique=True, nullable=False)


def actBase():
    URL = "sqlite:///botdata.sqlite"
    engine = sa.create_engine(URL)
    Base.metadata.create_all(engine)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    return session

def appendBase(session, subscription_name, subscriber_name, id, subscription_chat):
    if id not in session.query(BotData.id).all():
        try:
            sbs = BotData(id_sbs=id, subscriber_name=subscriber_name,subscription_name=subscription_name, subscription_chat=subscription_chat )
            session.add(sbs)
            session.commit()
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{subscription_name}: ', err, f' :{datetime.today()}', file=f)
    else:
        pass


def deleteBase(session, id, fname):
    appendBase(session, id, fname)
    path = os.getcwd() + f'\\usersMapping\\{id}'
    shutil.rmtree(path)
    delus = session.query(BotData).filter_by(id_sbs=id).first()
    session.delete(delus)
    session.commit()

def appenUsersChat(session, name, id, chat):
    if id not in session.query(BotDataUsersChat.id).all():
        try:
            sbs = BotDataUsersChat(id_sbs=id, name=name, chat=chat )
            session.add(sbs)
            session.commit()
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    else:
        pass


def deleteUsersChat(session, id, fname):
    appendBase(session, id, fname)
    path = os.getcwd() + f'\\usersChat\\{id}'
    shutil.rmtree(path)
    delus = session.query(BotDataUsersChat).filter_by(id_sbs=id).first()
    session.delete(delus)
    session.commit()

def close(session):
    session.close()
