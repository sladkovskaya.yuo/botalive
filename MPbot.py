import requests
import telebot
import os
import subprocess
import BotSql
from datetime import datetime
from emoji import emojize
import random
import keybord

token = ""
bot = telebot.TeleBot(token)
meanU = f'https://api.telegram.org/bot{token}'

@bot.message_handler(commands=['start','hello'])
def start_handler(message):
    bot.send_message(message.chat.id, f'''Добро пожаловать! Переживаете за ваших близких? 
    Я помогу вам всегда быть в курсе состояния тех, на кого вы подписались.
    Чтобы подписаться на кого-то отправьте команду /sayForMe имя_пользователя (через пробел, без @)''')

@bot.message_handler(commands=['sayForMe'])
def start_handler(message):
    session = BotSql.actBase()
    name = message.chat.username
    print(name)
    target = message.text.split()[1]
    try:
        BotSql.appendBase(session, target, name, target + '|' + name, message.chat.id)
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    bot.send_message(message.chat.id, "Вы подписались на " + target)
    BotSql.close(session)

@bot.message_handler(commands=['iAmHere'])
def start_handler(message):
    session = BotSql.actBase()
    name = message.chat.username
    try:
        BotSql.appenUsersChat(session, name, name, message.chat.id)
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    bot.send_message(message.chat.id, "Теперь на вас могут подписаться")
    BotSql.close(session)

@bot.message_handler(content_types=['photo'])
def text_handler(message):
    bot.send_message(message.chat.id, 'Ммм что это ты такое прислал, думаешь мне интересно?')

@bot.message_handler(content_types=['voice'])
def text_handler(message):
    bot.send_message(message.chat.id, 'Пока я не услышал твой чудесный голос, я до конца не мог поверить, что ты крыса)))0)')

@bot.message_handler(commands=['fine'])
def text_handler(message):
    bot.send_message(message.chat.id, f''' Скорее выбирай коробку и ищи там сырок, пока все не растащили... ''',
                     reply_markup=keybord.keybord_next([emojize(":package:", use_aliases=True),emojize(":package:", use_aliases=True), emojize(":package:", use_aliases=True),emojize(":package:", use_aliases=True)]))

@bot.message_handler(content_types=['text'])
def text_handler(message):
    if message.text == "📦":
        if (random.randint(1, 4) == 3):
            bot.send_message(message.chat.id,
                             'Вот это удача! Видно ты настоящий спец в поиске того, что кто-то удачно оставил без присмотра...')
            img = open(f"./cheese.jpg", 'rb')
            bot.send_photo(message.chat.id, img)
        else:
            bot.send_message(message.chat.id,
                         'Эх, а ты явно не мастер в поиске того, что кто-то удачно оставил без присмотра...')
            img = open(f"./bad.jpg", 'rb')
            bot.send_photo(message.chat.id, img)

bot.polling()
